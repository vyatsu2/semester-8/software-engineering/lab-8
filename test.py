import unittest

from black_box import BlackBox
from parameterized import parameterized, parameterized_class


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.black_box = BlackBox()

    @parameterized.expand([(0, 1, 1, 3, 0.5, 5),  # ТВ_1 (тестовый вариант - столбец 1)
                           (1, 1, 1, 3, 0.5, 4),  # ТВ_4 (x-2a=0)
                           (1, 0, 2, 2, 0.5, 0),  # ТВ_2
                           (0, 1, 0, 0, 0.5, 0),  # ТВ_3
                           (1, 1, 2, 2, 0.5, 0),  # ТВ_7
                           (1, 0, 1, 1, 0.5, 0),  # ТВ_8
                           (1, 1, 0, 0, 0.5, 0),  # ТВ_9
                           (0, 1, 1, 3.5, 0.5, 6),
                           (0, 1, 1, 3.6, 0.5, 6),  # верхняя чуть больше шага
                           (0, 0, 1, 3.5, 0.5, 0),  # деление на ноль, все точки выкалываются, массив пустой
                           (0, 1, 1, 1.1, 0.5, 1),  # отличие нижней от верхней меньше, чем шаг
                           (0, 1, 1, 1, 0.5, 1),  # верхняя и нижняя равны
                           ])
    def test_correct_len(self, a, b, x_min, x_max, dx, expected):
        print("test_correct_len", end=' ')
        result = self.black_box.compute(a, b, x_min, x_max, dx)
        print(result)
        self.assertEqual(expected, len(result))

    @parameterized.expand([(0, 1, 1, 3, 0.5, 1.0, -2.95970),
                           (0, 1, 1, 3, 0.5, 1.5, -2.78700),
                           (0, 1, 1, 3, 0.5, 3.0, -3.01072)
                           ])
    def test_correct_value(self, a, b, x_min, x_max, dx, check_key, expected):
        print("test_correct_value", end=' ')
        result = self.black_box.compute(a, b, x_min, x_max, dx)
        print(result)
        self.assertEqual(round(result[check_key], 5), expected)

    @parameterized.expand([(0, 1, 1, 3, 0.5, 0.9),  # ключ до нижней границы
                           (0, 1, 1, 3, 0.5, 3.1),  # ключ после верхней границы
                           ])
    def test_key_error(self, a, b, x_min, x_max, dx, check_key):
        print("test_key_error", end=' ')
        result = self.black_box.compute(a, b, x_min, x_max, dx)
        print(result)
        self.assertRaises(KeyError, lambda: result[check_key])

    @parameterized.expand([(0, 1, 3, 1, 0.5),  # ТВ_6 перепутаны границы
                           (0, 1, 1, 3, -0.5),  # ТВ_5 отрицательный шаг
                           ])
    def test_error_value(self, a, b, x_min, x_max, dx):
        self.assertRaises(AssertionError, lambda: self.black_box.compute(a, b, x_min, x_max, dx))


if __name__ == '__main__':
    unittest.main()
