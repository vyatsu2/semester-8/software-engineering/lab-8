import math
import numpy
class BlackBox:
    @staticmethod
    def compute(a, b, x_min, x_max, dx):
        assert (x_min <= x_max)
        assert (dx > 0)
        result = {}
        for x in numpy.arange(round(x_min, 3), round(x_max + dx/2, 3), round(dx, 3)):
            if x - 2 * a != 0 and x != 0 and b != 0:
                result[x] = math.cos((x - a) ** 2 / (x - 2 * a)) - 3.5 / math.sqrt(x * b)
        return result
